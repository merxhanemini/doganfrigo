<?php

/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
//add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );

/**
 * Dequeue the Storefront Parent theme core CSS
 */
function sf_child_theme_dequeue_style() {
    wp_dequeue_style( 'storefront-style' );
    wp_dequeue_style( 'storefront-woocommerce-style' );
}

/**
 * Note: DO NOT! alter or remove the code above this text and only add your custom PHP functions below this text.
 */

/********* CUSTOMIZATIONS ************/
/* Remove Add to cart button */
add_filter( 'woocommerce_is_purchasable', '__return_false');


/**
 * Functions hooked into storefront_header action
 *
 * @hooked storefront_header_container                 - 0
 * @hooked storefront_skip_links                       - 5
 * @hooked storefront_social_icons                     - 10
 * @hooked storefront_site_branding                    - 20
 * @hooked storefront_secondary_navigation             - 30
 * @hooked storefront_product_search                   - 40
 * @hooked storefront_header_container_close           - 41
 * @hooked storefront_primary_navigation_wrapper       - 42
 * @hooked storefront_primary_navigation               - 50
 * @hooked storefront_header_cart                      - 60
 * @hooked storefront_primary_navigation_wrapper_close - 68
 */

 add_action( 'init', 'remove_my_action');
 function remove_my_action() {
      remove_action( 'storefront_header','storefront_header_cart', 60 );
      remove_action( 'storefront_header', 'storefront_product_search', 40 );
      remove_action( 'storefront_header', 'storefront_secondary_navigation', 30 );
      add_action('storefront_header', 'storefront_secondary_navigation_child', -1);


      remove_action( 'storefront_header', 'storefront_primary_navigation_wrapper', 42 );
      remove_action( 'storefront_header', 'storefront_primary_navigation', 50 );
      remove_action( 'storefront_header', 'storefront_primary_navigation_wrapper_close', 68 );

      add_action( 'storefront_header', 'storefront_primary_navigation_wrapper', 19  );
      add_action( 'storefront_header', 'storefront_primary_navigation', 26 );
      add_action( 'storefront_header', 'storefront_primary_navigation_wrapper_close', 27  );



 }

add_action( 'wp_enqueue_scripts', 'storefront_social_icons_enqueue_fab' );
function storefront_social_icons_enqueue_fab() {
  wp_enqueue_style( 'font-awesome-5-brands', '//use.fontawesome.com/releases/v5.0.13/css/brands.css' );
 }



 // This theme uses wp_nav_menu() in two locations.
 register_nav_menus( apply_filters( 'storefront_register_nav_menus', array(
   'tertiary'   => __( 'Tertiary Menu', 'storefront-child' ),
 ) ) );

 /* Position the secondary menu on top */
 if ( ! function_exists( 'storefront_secondary_navigation_child' ) ) {
 	/**
 	 * Display Secondary Navigation
 	 *
 	 * @since  1.0.0
 	 * @return void
 	 */
 	function storefront_secondary_navigation_child() {
 	    if ( has_nav_menu( 'secondary' ) ) {
 		    ?>

 		    <nav class="secondary-navigation" role="navigation" aria-label="<?php esc_html_e( 'Secondary Navigation', 'storefront' ); ?>">
          <div class="col-full">
   			    <?php
   				    wp_nav_menu(
   					    array(
   						    'theme_location'	=> 'secondary',
   						    'fallback_cb'		=> '',
   					    )
   				    );

              wp_nav_menu(
   					    array(
   						    'theme_location'	=> 'tertiary',
   						    'fallback_cb'		=> '',
   					    )
   				    );
   			    ?>
          </div><!--.col-full-->
 		    </nav><!-- #site-navigation -->

 		    <?php
 		}
 	}
 }

